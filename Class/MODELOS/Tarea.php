<?php
    namespace Elearning\MODELOS;
    class Tarea  extends Conexion
    {
        public $id;
        public $id_clase;
        public $Unidad;
        public $Tema;
        public $Archivo;
        public $Fecha_Regis;
        public $Fecha_Entreg;

        function insert()
        {
            $this->Fecha_Regis = date ("Y-m-d");
            $this->Fecha_Entreg = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO Tarea( Unidad, Tema, Archivo) VALUES (?,?,?)");
            $pre -> bind_param("sss", $this->Unidad, $this->Tema, $this->Archivo, $this->Fecha_Regis, $this->Fecha_Entreg);
            $pre->execute(); 
        }

        static function findAll()
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM tarea");
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Tarea::class);
        }

        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM tarea WHERE id=¨?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Tarea::class);

        }
        
        public function update()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE tarea SET Unidad=?, Tema=?, Archivo=? WHERE id=?");
			$pre->bind_param("sssi", $this->Unidad, $this->Tema,$this->Archivo,$this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function delete($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM tarea WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}  
    }
?>