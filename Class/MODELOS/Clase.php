<?php
    namespace Elearning\MODELOS;
    class Clase extends Conexion
    {
        public $id;
        public $id_profesor;
        public $Nombre;
        public $Codigo;
        public $Fecha_Creacion;

        function insert()
        {
            $this->Fecha_Creacion = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO clase( Nombre, Codigo, Fecha_Creacion) VALUES (?,?,?)");
            $pre -> bind_param("sss", $this->Nombre, $this->Codigo, $this->Fecha_Creacion);
            $pre->execute(); 
        }

        static function findAll()
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM clase");
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Clase::class);
        }

        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM clase WHERE id=¨?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Clase::class);

        }
        
        public function update()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE clase SET Nombre=?, Codigo=? WHERE id=?");
			$pre->bind_param("ssi", $this->Nombre, $this->Matricula,$this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function delete($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM clase WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}  
    }
?>