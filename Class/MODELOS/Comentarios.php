<?php
    namespace Elearning\MODELOS;
    class Comentarios extends Conexion
    {
        public $id;
        public $Fecha;
        public $id_tarea
        public $id_alumno;
        public $comentario;
        public $tipo;
      
        function insert()
        {
            $this->Fecha = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO comentario( comentario, tipo, Fecha) VALUES (?,?,?)");
            $pre -> bind_param("sss", $this->Comentario, $this->tipo $this->Fecha);
            $pre->execute(); 
        }

        static function findAll()
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM comentario");
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Comentarios::class);
        }

        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM comentario WHERE id=¨?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Comentarios::class);

        }
        
        public function update()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE comentario SET comentario=?, tipo=? WHERE id=?");
			$pre->bind_param("ssi", $this->comentario, $this->tipo, $this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function delete($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM comentario WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}  
    }
?>