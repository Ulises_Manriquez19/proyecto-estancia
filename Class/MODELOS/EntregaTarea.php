<?php
    namespace Elearning\MODELOS;
    class EntregaTarea extends Conexion
    {
        public $id;
        public $id_tarea;
        public $id_alumno;
        public $Archivo;
        public $Fecha;

        function insert()
        {
            $this->Fecha = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO entregatarea( Archivo, Fecha) VALUES (?,?)");
            $pre -> bind_param("ss", $this->Archivo, $this->Fecha);
            $pre->execute(); 
        }

        static function findAll()
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM entregatarea");
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(EntregaTarea::class);
        }

        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM entregatarea WHERE id=¨?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(EntregaTarea::class);

        }
        
        public function update()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE entregatarea SET Archivo=? WHERE id=?");
			$pre->bind_param("si", $this->Archivo,$this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function delete($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM entregatarea WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}  
    }
?>