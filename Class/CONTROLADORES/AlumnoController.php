<?php 
	use ELearning\MODELOS\Alumno;
	include "MODELOS/Conexion.php";
	include "MODELOS/Alumno.php";
	class AlumnoController
	{ 
		function __construct()
		{
			 
		}

		function Create()
		{
			$alumno = new Alumno();
			$alumno->Matricula = $_POST["Matricula"];
			$alumno->Correo = $_POST["Correo"];
			$alumno->Nombre = $_POST["Nombre"];
			$alumno->A_Paterno = $_POST["A_Paterno"];
            $alumno->A_Materno = $_POST["A_Materno"];
			$alumno->Foto = $_POST["Foto"];
			$alumno->Contrasena = $_POST["Contrasena"];
			$alumno->insert();

			echo json_encode($alumno);
		}
		
		function buscarid()
        {
            $id = $_POST["id"];
            $usuario = Alumno::findid($id);
            echo json_encode($usuario);
        }

        function acceder()
        {
            $Correo = $_POST["Correo"];
            $Contrasena = $_POST["Contrasena"];
            $usuario = Alumno::access($Correo,$Contrasena);
            echo json_encode($usuario);
        }

        function correo()
        {
            $correo = $_POST["Correo"];
            $usuario = Alumno::email($correo);
            echo json_encode($usuario);
        }

        function eliminar()
        {
            $id = $_POST["id"];
            $usuario = Alumno::delete($id);
            return true;
        }

        function show()
        {
            $usuario = Alumno::findAll();
            echo json_encode($usuario);
        }
    }
?>