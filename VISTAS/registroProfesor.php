<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V3</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/ELEARNING/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/ELEARNING/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/ELEARNING/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/ELEARNING/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/ELEARNING/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/ELEARNING/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/ELEARNING/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/ELEARNING/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/ELEARNING/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/ELEARNING/CSS/util.css">
	<link rel="stylesheet" type="text/css" href="/ELEARNING/CSS/main2.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('/ELEARNING/images/profesor.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" id="InPro">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						register
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Matricula">
						<input class="input100" type="text" title="La matricula es requerida" required name="Matricula" placeholder="Matricula" id="Matricula">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Nombre">
						<input class="input100" type="text" required title="El nombre es requerido" name="Nombre" placeholder="Nombre" id="Nombre">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Apellido Paterno">
						<input class="input100" type="text" required title="El apellido paterno es requerido" name="A_Paterno" placeholder="Apellido paterno" id="A_Paterno">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Apellido Materno">
						<input class="input100" type="text" required title="El apellido materno es requerido" name="A_Materno" placeholder="Apellido Materno" id="A_Materno">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Correo">
						<input class="input100" type="email" required name="Correo" title="El correo es requerido" placeholder="Correo" id="Correo">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Contraseña">
						<input class="input100" type="password" required title="La contraseña es requerida" name="contrasena" placeholder="contraseña" id="Contrasena">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="fecha de nacimiento ">
						<input class="input100" type="date" required title="La fecha de nacimiento es requerida" name="Fech_Reg" placeholder="fecha de nacimiento" id="Fech_Reg">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
					<div class="container-login100-form-btn">
						<input type="submit" class="login100-form-btn" id="enviar">
                    </div>
                    
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>

	<script src="/ELEARNING/JS/jquery-3.5.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/ELEARNING/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/ELEARNING/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/ELEARNING/vendor/bootstrap/js/popper.js"></script>
	<script src="/ELEARNING/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/ELEARNING/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/ELEARNING/vendor/daterangepicker/moment.min.js"></script>
	<script src="/ELEARNING/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/ELEARNING/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/ELEARNING/JS/main.js"></script>
	<script src="/ELEARNING/JS/registroProfesor.js"></script>
</body>
</html>