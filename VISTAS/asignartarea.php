<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="/ELEARNING/CSS/asignaciones.css">
</head>
<body>
    <div class="contenido">
        <div>
            <table class="tabla">
                <tr>
                    <td colspan="4"><p><h1>Asignar Tarea</h1></p></td>
                    <td>
                        <input type="button" value="Guardar como borrador" class="Guar">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="">Titulo de la tarea</label>
                    </td>
                    <td>
                        <input type="text" class="entrada">
                    </td>
                    <td>
                        <label for="">Calificacion</label>
                    </td>
                    <td colspan="2">
                        <input type="number" value="Calificacion" class="entrada">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="">Instrucciones</label>
                    </td>
                    <td>
                        <input type="text" placeholder="(Opcional)" class="entrada-grd">
                    </td>
                    <td>
                        <label for="">Fecha de entrega</label>
                    </td>
                    <td>
                        <input type="date" class="entrada">
                    </td>
                    <td>
                        <label for="">Hora de entrega (opcional)</label>
                    </td>
                    <td>
                        <input type="text-area" class="entrada">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input type="file" value="Añadir">
                    </td>
                    <td>
                        <input type="button" value="Guardar" class="Guar">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>