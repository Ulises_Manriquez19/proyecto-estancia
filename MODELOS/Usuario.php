<?php
    namespace E_learning\MODELOS;
    class Usuario extends Conexion
    {
        public $id;
        public $Nombre;
        public $ApellidoPat;
        public $ApellidoMat;
        public $Correo;
        public $Contrasena;
        public $FechaNac;
        public $Sexo;
        public $Foto;
        public $FechaReg;

        function insert()
        {
            $this->FechaReg = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO usuario (Nombre, ApellidoPat, ApellidoMat, Correo, Contrasena, FechaNac, Sexo, FechaReg,Foto) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)");
            $pre -> bind_param("sssssssss", $this->Nombre, $this->ApellidoPat, $this->ApellidoMat, $this->Correo, $this->Contrasena, $this->FechaNac, $this->Sexo, $this->FechaReg,$this->Foto);
            $pre -> execute();
            $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id");
            $pre_->execute();
            $r = $pre_->get_result();
            $this->id = $r->fetch_assoc()["id"];
            return true; 
        }

        static function access($Nombre)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM usuario WHERE Nombre=?");
			$pre->bind_param("s",$Nombre);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_assoc();
		}
        
        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM  usuario WHERE id=¨?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Usuario::class);

        }

        static function findbyemail($Correo)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM usuario WHERE Correo=?");
			$pre->bind_param ("s", $Correo);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Usuario::class);
        }
        
        public function editar()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE usuario SET Nombre=?,Correo=?,Contrasena=?,ApellidoPat=?,ApellidoMat=?,FechaNac=?,Sexo=?,FechaReg=?,Foto=? WHERE id=?");
			$pre->bind_param("sssssssssi", $this->Nombre, $this->ApellidoPat, $this->ApellidoMat, $this->Correo, $this->Contrasena, $this->FechaNac, $this->Sexo, $this->FechaReg,$this->Foto,$this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function eliminar($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM usuario WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}


           
    }
?>