<?php
    namespace ELearning\MODELOS;
    class Tarea  extends Conexion
    {
        public $Id;
        public $Id_clase;
        public $Unidad;
        public $Tema;
        public $Archivo;
        public $Fech_Regis;
        public $Fech_Entreg;

        function insert()
        {
            $this->Fech_Regis = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO tarea (Id_clase, Unidad, Tema, Archivo, Fech_Regis, Fech_Entreg ) VALUES (?,?,?,?,?,?)");
            $pre-> bind_param("isssss",$this->Id_clase, $this->Unidad, $this->Tema, $this->Archivo, $this->Fech_Regis, $this->Fech_Entreg);
            $pre->execute();
        }

        static function findAll()
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM tarea");
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Tarea::class);
        }

        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM tarea WHERE id=¨?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Tarea::class);

        }
        
        public function update()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE tarea SET Unidad=?, Tema=?, Archivo=? WHERE id=?");
			$pre->bind_param("sssi", $this->Unidad, $this->Tema,$this->Archivo,$this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function delete($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM tarea WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}  
    }
?>