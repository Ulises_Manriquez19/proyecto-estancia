<?php
    namespace ELearning\MODELOS;
    class Clases extends Conexion
    {
        public $Id_clase;
        public $Id_alumno;

        function insert()
        {
            $pre = mysqli_prepare($this->con,"INSERT INTO clases(Id_clase, Id_alumno) VALUES (?,?)");
            $pre -> bind_param("ii", $this->Id_clase, $this->Id_alumno);
            $pre -> execute();
        }

        static function findstudent($Id_alumno)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM clases,clase WHERE clases.Id_alumno=?");
			$pre->bind_param("i",$Id_alumno);
			$pre->execute();
			$res=$pre->get_result();
			$clases=[];
			while ($clase = $res->fetch_object(Clases::class)) 
			{
				array_push($clases, $clase);
			}
			return $clases;
		}
        
        static function findclass($Id_clase)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM  clases WHERE Id_clase=¨?");
            $pre->bind_param("i",$Id_clase);
            $pre->execute();
            $res=$pre->get_result();
            return $res->fetch_object(Clases::class);

        }
        
        public function editar($Id_clase, $Id_alumno)
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE clases SET Id_clase=? WHERE Id_alumno=?");
			$pre->bind_param("ii", $this->Id_clase, $this->Id_alumno);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function delete($Id_clase, $Id_alumno)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM clases WHERE Id_clase=? AND Id_alumno=?");
			$pre->bind_param ("ii", $Id_clase, $Id_alumno);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}


           
    }
?>