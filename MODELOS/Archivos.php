<?php
    namespace Elearning\MODELOS;
    class Archivos extends Conexion
    {
        public $id;
       

        function insert()
        {
            $this->FechaReg = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO profesor (Matricula, Correo, Nombre, A_Paterno, A_Materno, Foto, Contrasena, Fech_Reg) VALUES (?,?,?,?,?,?,?,?)");
            $pre->bind_param("ssssssss", $this->Matricula, $this->Correo, $this->Nombre, $this->A_Paterno, $this->A_Materno, $this->Foto, $this->Contrasena, $this->FechaReg);
            $pre->execute(); 
        }

        static function findAll()
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM profesor");
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Archivos::class);
        }

        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM profesor WHERE id=¨?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Archivos::class);

        }

        static function name($Nombre)
        {
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT * FROM profesor WHERE Nombre=?");
            $pre->bind_param ("s", $Nombre);
            $pre->execute();
            $res=$pre->get_result();
            return $res->fetch_object(Archivos::class);
        }

        static function access($Correo,$Contrasena)
        {
            $me = new Conexion();
            $pre = mysqli_prepare($me->con, "SELECT * FROM profesor WHERE Correo=? AND Contrasena=?");
            $pre->bind_param("ss", $Correo, $Contrasena);
            $pre->execute();
            $res=$pre->get_result();
            return $res->fetch_object(Archivos::class);
        }

        static function email($Correo)
        {
            $me = new Conexion();
            $pre = mysqli_prepare($me->con, "SELECT * FROM profesor WHERE Correo=?");
            $pre->bind_param("s", $Correo);
            $pre->execute();
            $res = $pre->get_result();
            return $res->fetch_object(Archivos::class);
        }
        
        public function edit()
        {
            $me =new Conexion();
            $pre=mysqli_prepare($me->con,"UPDATE profesor SET Matricula=?, Correo=?, Nombre=?, A_Paterno=?, A_Materno=?, Foto=?, Contrasena=? WHERE id=?");
            $pre->bind_param("sssssssi", $this->Matricula, $this->Correo, $this->Nombre, $this->A_Paterno, $this->A_Materno, $this->Foto,$this->id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }

        static function delete($id)
        {
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"DELETE FROM alumno WHERE id=?");
            $pre->bind_param ("i", $id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }  
    }
?>