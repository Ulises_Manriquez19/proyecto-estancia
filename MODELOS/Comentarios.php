<?php
    namespace Elearning\MODELOS;
    class Comentarios extends Conexion
    {
        public $id;
        public $Fecha;
        public $Id_tarea;
        public $Id_alumno;
        public $Comentario;
        public $Tipo;
      
        function insert()
        {
            $this->Fecha = date ("Y-m-d");
            $pre = mysqli_prepare($this->con,"INSERT INTO comentario(Fecha, Id_tarea, Id_alumno, Comentario, Tipo) VALUES (?,?,?,?,?)");
            $pre -> bind_param("siiss",$this->Fecha,$this->Id_tarea, $this->Id_alumno, $this->Comentario, $this->Tipo);
            $pre->execute(); 
        }

        static function findAll()
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM comentario");
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Comentarios::class);
        }

        static function findid($id)
        {
            $cn = new Conexion();
            $pre = mysqli_prepare($cn->con,"SELECT * FROM comentario WHERE Id=¨?");
            $pre->bind_param("i",$Id);
            $pre->execute();
            $res=$pre->get_result();

            return $res->fetch_object(Comentarios::class);

        }
        
        public function update()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE comentario SET Comentario=?, Tipo=? WHERE id=?");
			$pre->bind_param("ssi", $this->Comentario, $this->Tipo, $this->Id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function delete($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM comentario WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}  
    }
?>