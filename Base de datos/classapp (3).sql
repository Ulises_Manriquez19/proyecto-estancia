-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-07-2020 a las 17:19:34
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `classapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `Id` int(4) NOT NULL,
  `Matricula` int(10) NOT NULL,
  `Correo` varchar(20) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `A_Paterno` varchar(30) NOT NULL,
  `A_Materno` varchar(30) NOT NULL,
  `Foto` varchar(50) DEFAULT NULL,
  `Contrasena` varchar(20) NOT NULL,
  `Fech_Reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`Id`, `Matricula`, `Correo`, `Nombre`, `A_Paterno`, `A_Materno`, `Foto`, `Contrasena`, `Fech_Reg`) VALUES
(1, 1318094412, 'saul@gmail.com', 'Saul', 'Cigales', 'Pina', 'foto', 'Grandisimo', '2020-07-21 00:00:00'),
(2, 123456, 'coco_uliseses@hotmai', 'juan', 'manriquez', 'rodriguez', 'F', '123juan', '2020-07-31 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase`
--

CREATE TABLE `clase` (
  `Id` int(4) NOT NULL,
  `Id_profesor` int(4) DEFAULT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `Codigo` varchar(10) DEFAULT NULL,
  `Fech_Creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clase`
--

INSERT INTO `clase` (`Id`, `Id_profesor`, `Nombre`, `Codigo`, `Fech_Creacion`) VALUES
(1, 1, 'Español', '156186', '2020-07-27 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE `clases` (
  `Id_clase` int(4) DEFAULT NULL,
  `Id_alumno` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clases`
--

INSERT INTO `clases` (`Id_clase`, `Id_alumno`) VALUES
(1, 1),
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `Id` int(4) NOT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Id_tarea` int(4) DEFAULT NULL,
  `Id_alumno` int(4) DEFAULT NULL,
  `Comentario` text DEFAULT NULL,
  `Tipo` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`Id`, `Fecha`, `Id_tarea`, `Id_alumno`, `Comentario`, `Tipo`) VALUES
(0, '2020-07-28 00:00:00', 1, 1, 'Hola', 'vip');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entregatarea`
--

CREATE TABLE `entregatarea` (
  `Id` int(4) NOT NULL,
  `Id_tarea` int(4) DEFAULT NULL,
  `Id_alumno` int(4) DEFAULT NULL,
  `Archivo` text DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `entregatarea`
--

INSERT INTO `entregatarea` (`Id`, `Id_tarea`, `Id_alumno`, `Archivo`, `Fecha`) VALUES
(1, NULL, NULL, 'hghk', '2020-07-28 00:00:00'),
(2, 1, 1, 'hghk', '2020-07-28 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `Id_profesor` int(4) NOT NULL,
  `Matricula` int(15) NOT NULL,
  `Correo` varchar(20) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `A_Paterno` varchar(30) NOT NULL,
  `A_Materno` varchar(30) NOT NULL,
  `Foto` varchar(50) DEFAULT NULL,
  `Contrasena` varchar(20) NOT NULL,
  `Fech_Reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`Id_profesor`, `Matricula`, `Correo`, `Nombre`, `A_Paterno`, `A_Materno`, `Foto`, `Contrasena`, `Fech_Reg`) VALUES
(1, 1318094412, 'saul@gmail.com', 'Saul', 'Cigales', 'Pina', 'foto', 'Grandisimo', '2020-07-23 00:00:00'),
(2, 192123, 'coco_ulise@hotmail.c', 'ulises', 'manriquez', 'romero', NULL, 'puchis', '2020-07-31 00:00:00'),
(3, 192123, 'coco_ulise@hotmail.c', 'ulises', 'manriquez', 'romero', 'F', 'puchis', '2020-07-31 00:00:00'),
(4, 0, '', '', '', '', 'F', '', '2020-07-31 00:00:00'),
(5, 0, '', '', '', '', 'F', '', '2020-07-31 00:00:00'),
(6, 192123, 'coco_ulise@hotmail.c', 'ulises', 'romero', 'manriquez', 'F', 'pichis', '2020-07-31 00:00:00'),
(7, 2147483647, 'coco_ulise@hotmail.c', 'ulises', 'manriquez', 'manriquez', 'F', 'puchis', '2020-07-31 00:00:00'),
(8, 1318094395, 'rodriguez95@gmai.com', 'juan', 'perez', 'rodriguez', 'F', 'jaun123', '2020-07-31 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE `tarea` (
  `Id` int(4) NOT NULL,
  `Id_clase` int(4) NOT NULL,
  `Unidad` varchar(30) DEFAULT NULL,
  `Tema` varchar(35) DEFAULT NULL,
  `Archivo` text DEFAULT NULL,
  `Fech_Regis` datetime DEFAULT NULL,
  `Fech_Entreg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`Id`, `Id_clase`, `Unidad`, `Tema`, `Archivo`, `Fech_Regis`, `Fech_Entreg`) VALUES
(1, 1, NULL, NULL, 'uyhjgbjhsebk', '2020-07-28 00:00:00', '2020-07-21 00:00:00'),
(2, 1, 'Uno', 'Calculo', 'uyhjgbjhsebk', '2020-07-28 00:00:00', '2020-07-21 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `clase`
--
ALTER TABLE `clase`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_profesor` (`Id_profesor`);

--
-- Indices de la tabla `clases`
--
ALTER TABLE `clases`
  ADD KEY `Id_clase` (`Id_clase`),
  ADD KEY `Id_alumno` (`Id_alumno`) USING BTREE;

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_tarea` (`Id_tarea`),
  ADD KEY `Id_alumno` (`Id_alumno`);

--
-- Indices de la tabla `entregatarea`
--
ALTER TABLE `entregatarea`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_tarea` (`Id_tarea`),
  ADD KEY `Id_alumno` (`Id_alumno`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`Id_profesor`);

--
-- Indices de la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_clase` (`Id_clase`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `clase`
--
ALTER TABLE `clase`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `entregatarea`
--
ALTER TABLE `entregatarea`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `Id_profesor` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tarea`
--
ALTER TABLE `tarea`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clase`
--
ALTER TABLE `clase`
  ADD CONSTRAINT `clase_ibfk_1` FOREIGN KEY (`Id_profesor`) REFERENCES `profesor` (`Id_profesor`);

--
-- Filtros para la tabla `clases`
--
ALTER TABLE `clases`
  ADD CONSTRAINT `clases_ibfk_1` FOREIGN KEY (`Id_clase`) REFERENCES `clase` (`Id`),
  ADD CONSTRAINT `clases_ibfk_2` FOREIGN KEY (`Id_alumno`) REFERENCES `alumno` (`Id`);

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `comentario_ibfk_1` FOREIGN KEY (`Id_tarea`) REFERENCES `tarea` (`Id`),
  ADD CONSTRAINT `comentario_ibfk_2` FOREIGN KEY (`Id_alumno`) REFERENCES `alumno` (`Id`);

--
-- Filtros para la tabla `entregatarea`
--
ALTER TABLE `entregatarea`
  ADD CONSTRAINT `entregatarea_ibfk_1` FOREIGN KEY (`Id_tarea`) REFERENCES `tarea` (`Id`),
  ADD CONSTRAINT `entregatarea_ibfk_2` FOREIGN KEY (`Id_alumno`) REFERENCES `alumno` (`Id`);

--
-- Filtros para la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD CONSTRAINT `tarea_ibfk_1` FOREIGN KEY (`Id_clase`) REFERENCES `clase` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
