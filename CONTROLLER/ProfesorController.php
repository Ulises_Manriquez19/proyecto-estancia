<?php 
	use ELearning\MODELOS\Profesor;
	include "MODELOS/Conexion.php";
	include "MODELOS/Profesor.php";
	class ProfesorController
	{ 
		function __construct()
		{
			 
		}

		function Create()
		{
			$profesor = new Profesor();
			$profesor->Matricula = $_POST["Matricula"];
			$profesor->Correo = $_POST["Correo"];
			$profesor->Nombre = $_POST["Nombre"];
			$profesor->A_Paterno = $_POST["A_Paterno"];
            $profesor->A_Materno = $_POST["A_Materno"];
			$profesor->Foto = "F";
			$profesor->Contrasena = $_POST["contrasena"];
			$profesor->insert();

			echo json_encode(["estado"=>true,"detalle"=>$profesor]);
		}
		
		function buscarid()
        {
            $id = $_POST["id"];
            $usuario = Profesor::findid($id);
            echo json_encode($usuario); 
        }

        function acceder()
        {
            $Correo = $_POST["Correo"];
            $Contrasena = $_POST["Contrasena"];
            $usuario = Profesor::access($Correo,$Contrasena);
            echo json_encode($usuario);
        }

        function correo()
        {
            $correo = $_POST["Correo"];
            $usuario = Profesor::email($correo);
            echo json_encode($usuario);
        }

        function eliminar()
        {
            $id = $_POST["id"];
            $usuario = Profesor::delete($id);
            return true;
        }

        function show()
        {
            $usuario = Profesor::findAll();
            echo json_encode($usuario);
        }
    }
?>