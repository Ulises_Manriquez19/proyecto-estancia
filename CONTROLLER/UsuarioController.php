<?php 
	use E_learning\MODELOS\Usuario;
	include "Modelo/Conexion.php";
	include "Modelo/Usuario.php";
	class UsuariosController
	{ 
		function __construct()
		{
			 
		}
		
		function acceder()
		{
			$Nombre=$_POST["Nombre"];
			$Correo=$_POST["Correo"];
			$Contrasena=$_POST["Contrasena"];
			$usuario=Usuario::access($NomUsuario);
			if($usuario['Nombre']==$Nombre && $usuario['Correo']==$Correo && $usuario['Contrasena']==$Contrasena)
			{
				session_start();
				$user=$_SESSION['usuario']=$NomUsuario;
				echo json_encode($usuario);
			}
			else
			{
				echo json_encode(["estado"=>false]);
			}
		}
		
			public function create()
		{
			
			if(isset($_POST["Nombre"])&& isset($_POST["Contrasena"])&& isset($_POST["Correo"])&& isset($_POST["ApellidoPat"])&& isset($_POST["ApellidoMat"])&& isset($_POST["FechaNac"])&& isset($_POST["Sexo"])&& isset($_POST["FechaReg"]))
			{
				$usuario=new Usuario();
				$Nombre=$_POST["Nombre"];
				$ApellidoPat=$_POST["ApellidoPat"];
				$ApellidoMat=$_POST["ApellidoMat"];
				$Correo=$_POST["Correo"];
				$Contrasena=$_POST["Contrasena"];
				$FechaNac=$_POST["FechaNac"];
				$Foto=$_POST["Foto"];
        		$Sexo=$_POST["Sexo"];
        		$FechaReg=$_POST["FechaReg"];
				

				$usuario->insert();
				echo json_encode(["estatus"=>true,"usuario"=>$usuario]);
				
			}
			else
			{
				echo json_encode(["estatus"=>false,"message"=>"error"]);
			}
		}
		
		public function find()
		{	
			$id=$_POST["id"];
			$usuario=new Usuario();
			$usuario=$usuario::find($id);
			if ($id==true)
			{
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}
			else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
			
			
		}

		public function findemail()
		{
			if(isset($_POST["Correo"]))
			{
				$Correo=$_POST["Correo"];
				$usuario=new Usuario();
				$usuario=$usuario->findbyemail($Correo);
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}
			else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}	
		}

		public function edit()
		{
			if(isset($_POST["Nombre"])&& isset($_POST["Contrasena"])&& isset($_POST["Correo"])&& isset($_POST["ApellidoPat"])&& isset($_POST["ApellidoMat"])&& isset($_POST["FechaNac"])&& isset($_POST["Sexo"])&& isset($_POST["FechaReg"])&& isset($_POST["Foto"]))
			{
				$usuario=new Usuario();
				$Nombre=$_POST["Nombre"];
				$ApellidoPat=$_POST["ApellidoPat"];
				$ApellidoMat=$_POST["ApellidoMat"];
				$Correo=$_POST["Correo"];
				$Contrasena=$_POST["Contrasena"];
				$FechaNac=$_POST["FechaNac"];
				$Sexo=$_POST["Sexo"];
				$Foto=$_POST["Foto"];
        		$FechaReg=$_POST["FechaReg"];
				$usuario->id=$_POST["id"];
				$usuario->editar();
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
		}

		public function delete()
		{
			if (isset($_POST["id"])) 
			{
				$id=$_POST["id"];
				$usuario=new Usuario();
				$usuario=$usuario->eliminar($id);
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}else{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
		}	
	}
?>