<?php
    if(isset($_GET["controller"])&&isset($_GET["action"]))
    {
        $controller = $_GET["controller"];
        $action = $_GET["action"];
        session_start();
        $clase = $controller . "Controller";

        require_once ("CONTROLLER/".$clase.".php");

        $instancia = new $clase();
        $instancia->{$action}();
    }
    else
    {
        echo "Peticion invalida";
        echo "<h1>Error 404</h1>";
    }
?>